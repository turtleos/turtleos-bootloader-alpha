import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {useSelector} from 'react-redux';

export default function BootloaderView() {
    const vfs = useSelector(state=>state.vfs);
    console.log(vfs.readFileSync('/etc/hostname'));
  return (
    <View style={styles.container}>
      <Text>{vfs.readFileSync('/etc/hostname').data.content}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
